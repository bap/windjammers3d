#include "WindMaths.h"

Real solvePolynom(Real a, Real b, Real c)
{
  Real delta = pow(b, 2) - 4 * a * c;

  if (a == 0.0)
    a = 0.1;

  if (delta == 0.0)
    return (-b / (2 * a));
  else
    return (-b + (rand() % 2 == 0 ? -sqrt(delta) : sqrt(delta))) / (2 * a);
}

Real findYCircle(Real rayon, Real centerX, Real centerY, Real x)
{
  return solvePolynom(1, (-2 * centerY), -(pow(rayon, 2) + pow(centerY, 2) - pow(x - centerX, 2)));
}

bool areEqualRel(float a, float b, float epsilon)
{
    return (fabs(a - b) <= epsilon * std::max(fabs(a), fabs(b)));
}