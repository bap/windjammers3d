#ifndef __WindMaths_h_
#define __WindMaths_h_

#include <cmath>
#include <OgreVector3.h>

using namespace Ogre;

Real solvePolynom(Real a, Real b, Real c);
Real findYCircle(Real rayon, Real centerX, Real centerY, Real x);
bool areEqualRel(float a, float b, float epsilon);

#endif // #ifndef __WindMaths_h_