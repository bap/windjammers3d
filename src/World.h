#ifndef WORLD_H_
# define WORLD_H_

# include "Player.h"

# define DISC_DEFAULT 0
# define DISC_LOB     0x1
# define DISC_FLIP    0x2

struct Disc
{
    Ogre::Vector3    mSpeed;
    Ogre::Vector3    startLob;
    Ogre::Vector3    endLob;
    Ogre::Quaternion mOrientation;
    char             state;
    bool             lob;
    bool             mFlip;
};

struct World
{
    Disc   *mDisc;
    Player *mPlayer1;
    Player *mPlayer2;
    float mTimer;
};

#endif /* !WORLD_H_ */
