#ifndef GAME_H
# define GAME_H

# include "OgreToolbox.h"
# include "input/InputManager.h"
# include "utils/WindMaths.h"
# include "World.h"

class Game : public Ogre::Singleton<Game>,
             public Ogre::FrameListener
{
public:
    Game();
    ~Game();

    void startGame();
    // This method is public because of the workaround needed to make the
    // application work on OS X (see AppDelegate.h)
    // All the logic should be done in this method and not in runGame()
    void updateGame(double timeSinceLastFrame);
    
    // Update score on player objects and GUI
    void updateScore(bool inPlayer1);
    void initGame(bool initSet);
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

    void shutDown() { m_bShutdown = true; }
    bool isShuttingDown() { return m_bShutdown; }


    World *m_pWorld;
    bool mPlaying;
    bool mTraveling;

private:
    bool initOgre(Ogre::String wndTitle);
    void initInputs();
    void initResources(OgreToolbox& rOgreTb);
    void initKeyMapping();
    void setupGameScene();
#if !((OGRE_PLATFORM == OGRE_PLATFORM_APPLE) && __LP64__)
    void runGame();
#endif
    bool processUnbufferedInput(const Ogre::FrameEvent& evt, Player* player);
    bool collision(Ogre::SceneNode* Pl,
                   Ogre::SceneNode* Disc,
                   bool             flip);

    Input::InputManager *m_pInputMgr;

    bool m_bShutdown;
};

#endif /* GAME_H */
