#include "Game.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
# include "macUtils.h"
#endif

#include <sstream>

#define TER_HEIGHT 120
#define TER_WIDTH 240
#define MESH_SCALE 0.1
#define PLAYER_WIDTH 100
#define DISC_WIDTH 116
#define MIN_SPEED 170.0
#define MAX_SPEED 330.0
#define INIT_SPEED 100.0
#define BEZIER 130.0
#define FLIP_RAY 25

namespace Ogre
{
    template<> Game* Ogre::Singleton<Game>::msSingleton = 0;
};

Game::Game()
: m_pWorld    (new World),
m_pInputMgr (0),
m_bShutdown (false),
mTraveling (true)
{
    m_pWorld->mDisc = new Disc;
    m_pWorld->mDisc->state = DISC_DEFAULT;
}

Game::~Game()
{
    if (m_pInputMgr)
        delete m_pInputMgr;
    if (m_pWorld)
    {
        delete m_pWorld->mDisc;
        delete m_pWorld->mPlayer1;
        delete m_pWorld->mPlayer2;
        delete m_pWorld;
    }

    delete OgreToolbox::getSingletonPtr();
}

void Game::initInputs()
{
    unsigned long hWnd = 0;
    OIS::ParamList paramList;
    OgreToolbox& rOgreTb = OgreToolbox::getSingleton();

    rOgreTb.m_pRenderWnd->getCustomAttribute("WINDOW", &hWnd);

    paramList.insert(OIS::ParamList::value_type("WINDOW",
                                                Ogre::StringConverter::toString(hWnd)));

    m_pInputMgr = new Input::InputManager(paramList);
    m_pInputMgr->createKeyboard();
    m_pInputMgr->createMouse(rOgreTb.m_pRenderWnd->getHeight(),
                             rOgreTb.m_pRenderWnd->getWidth());
}

void Game::initResources(OgreToolbox& rOgreTb)
{
    Ogre::String secName, typeName, archName;
    Ogre::ConfigFile cf;
    cf.load(rOgreTb.m_ResourcePath + "resources.cfg");

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
            // OS X does not set the working directory relative to the app,
            // In order to make things portable on OS X we need to provide
            // the loading with it's own bundle path location

            // only adjust relative dirs
            if (!Ogre::StringUtil::startsWith(archName, "/", false))
                archName = Ogre::String(rOgreTb.m_ResourcePath + archName);
#endif
            Ogre::ResourceGroupManager::getSingleton()
            .addResourceLocation(archName, typeName, secName);
        }
    }
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

namespace
{
    void initCameraAndViewport(OgreToolbox& rOgreTb)
    {
        rOgreTb.m_pSceneMgr = rOgreTb.m_pRoot->createSceneManager(Ogre::ST_GENERIC,
                                                                  "SceneManager");
        rOgreTb.m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

        rOgreTb.m_pCamera = rOgreTb.m_pSceneMgr->createCamera("Camera");
        rOgreTb.m_pCamera->setPosition(Ogre::Vector3(0, 500, 1000));
        rOgreTb.m_pCamera->lookAt(Ogre::Vector3(0, 0, 0));
        rOgreTb.m_pCamera->setNearClipDistance(5);

        rOgreTb.m_pViewport = rOgreTb.m_pRenderWnd->addViewport(rOgreTb.m_pCamera);
        rOgreTb.m_pViewport->setBackgroundColour(Ogre::ColourValue(1.0f, 1.0f, 1.0f, 1.0f));

        rOgreTb.m_pCamera->setAspectRatio(Ogre::Real(rOgreTb.m_pViewport->getActualWidth())
                                          / Ogre::Real(rOgreTb.m_pViewport->getActualHeight()));

        rOgreTb.m_pViewport->setCamera(rOgreTb.m_pCamera);
    }

}

bool Game::initOgre(Ogre::String wndTitle)
{
    OgreToolbox& rOgreTb = OgreToolbox::getSingleton();
    new Ogre::LogManager();

    rOgreTb.m_pLog = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log",
                                                                true, true, false);
    rOgreTb.m_pLog->setDebugOutputEnabled(true);

    Ogre::String pluginsPath;
    // only use plugins.cfg if not static
#ifndef OGRE_STATIC_LIB
    pluginsPath = rOgreTb.m_ResourcePath + "plugins.cfg";
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
    rOgreTb.m_pRoot = new Ogre::Root(pluginsPath, Ogre::macBundlePath() + "/ogre.cfg");
#else
    rOgreTb.m_pRoot = new Ogre::Root(pluginsPath, "../config/ogre.cfg");
#endif

#ifdef OGRE_STATIC_LIB
    rOgreTb.m_StaticPluginLoader.load();
#endif

    if(!rOgreTb.m_pRoot->restoreConfig() && !rOgreTb.m_pRoot->showConfigDialog())
        return false;

    rOgreTb.m_pRenderWnd = rOgreTb.m_pRoot->initialise(true, wndTitle);

    initCameraAndViewport(rOgreTb);
    initInputs();
    initKeyMapping();
    initResources(rOgreTb);
    rOgreTb.m_pRoot->addFrameListener(this);

    rOgreTb.m_pTimer = OGRE_NEW Ogre::Timer();
    rOgreTb.m_pTimer->reset();

    if (m_pInputMgr)
    {
        rOgreTb.m_pRenderWnd->setActive(true);

        return true;
    }
    else
        return false;
}

namespace
{
    void reinitGame (World *world)
    {
        Game::getSingleton().initGame(true);
    }

    void shutdown (World *world)
    {
        if (!Game::getSingletonPtr()->mPlaying)
            Game::getSingletonPtr()->shutDown();
        else
        {
            Game::getSingletonPtr()->mPlaying = false;
        }
    }

    void viewOne (World *world)
    {
        OgreToolbox::getSingleton().m_pCamera->setPosition(Ogre::Vector3(0,250,130));
        OgreToolbox::getSingleton().m_pCamera->lookAt(Ogre::Vector3(0,0,0));
    }

    void viewTwo (World *world)
    {
        OgreToolbox::getSingleton().m_pCamera->setPosition(Ogre::Vector3(0,250,-130));
        OgreToolbox::getSingleton().m_pCamera->lookAt(Ogre::Vector3(0,0,0));
    }

    void viewThree (World *world)
    {
        OgreToolbox::getSingleton().m_pCamera->setPosition(Ogre::Vector3(-200,250,0));
        OgreToolbox::getSingleton().m_pCamera->lookAt(Ogre::Vector3(0,0,0));
    }

    void viewFour (World *world)
    {
        OgreToolbox::getSingleton().m_pCamera->setPosition(Ogre::Vector3(200,250,0));
        OgreToolbox::getSingleton().m_pCamera->lookAt(Ogre::Vector3(0,0,0));
    }

    void viewFive (World *world)
    {
        OgreToolbox::getSingleton().m_pCamera->setPosition(Ogre::Vector3(0,250,1));
        OgreToolbox::getSingleton().m_pCamera->lookAt(Ogre::Vector3(0,0,0));
    }

    void player1UpDownRelease (World *world)
    {
        world->mPlayer1->mSpeed.z = 0;
    }

    void player2UpDownRelease (World *world)
    {
        world->mPlayer2->mSpeed.z = 0;
    }

    void player1RightLeftRelease (World *world)
    {
        world->mPlayer1->mSpeed.x = 0;
    }

    void player2RightLeftRelease (World *world)
    {
        world->mPlayer2->mSpeed.x = 0;
    }
}

void Game::initKeyMapping()
{
    m_pInputMgr->mapKey(OIS::KC_R,      reinitGame, 0);
    m_pInputMgr->mapKey(OIS::KC_ESCAPE, shutdown,   0);
    m_pInputMgr->mapKey(OIS::KC_1,      viewOne,    0);
    m_pInputMgr->mapKey(OIS::KC_2,      viewTwo,    0);
    m_pInputMgr->mapKey(OIS::KC_3,      viewThree,  0);
    m_pInputMgr->mapKey(OIS::KC_4,      viewFour,   0);
    m_pInputMgr->mapKey(OIS::KC_5,      viewFive,   0);


#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE
    m_pInputMgr->mapKey(OIS::KC_Z,     0, player1UpDownRelease);
    m_pInputMgr->mapKey(OIS::KC_Q,     0, player1RightLeftRelease);
#else
    m_pInputMgr->mapKey(OIS::KC_W,     0, player1UpDownRelease);
    m_pInputMgr->mapKey(OIS::KC_A,     0, player1RightLeftRelease);
#endif
    m_pInputMgr->mapKey(OIS::KC_UP,    0, player2UpDownRelease);
    m_pInputMgr->mapKey(OIS::KC_S,     0, player1UpDownRelease);
    m_pInputMgr->mapKey(OIS::KC_DOWN,  0, player2UpDownRelease);
    m_pInputMgr->mapKey(OIS::KC_LEFT,  0, player2RightLeftRelease);
    m_pInputMgr->mapKey(OIS::KC_D,     0, player1RightLeftRelease);
    m_pInputMgr->mapKey(OIS::KC_RIGHT, 0, player2RightLeftRelease);
}

void Game::startGame()
{
    new OgreToolbox();
    if(!initOgre("WindJammers3D v0.1"))
        return;

    OgreToolbox::getSingletonPtr()->m_pLog->logMessage("Game initialized!");

    setupGameScene();
#if !((OGRE_PLATFORM == OGRE_PLATFORM_APPLE) && __LP64__)
    runGame();
#endif
}

void Game::setupGameScene()
{
    using namespace Ogre;
    OgreToolbox &rOgreTb = OgreToolbox::getSingleton();

    rOgreTb.m_pSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
    rOgreTb.m_pSceneMgr->setShadowColour( ColourValue(0.5, 0.5, 0.5) );

    // Create 2 Players
    m_pWorld->mPlayer1 = new Player("Player1", rOgreTb.m_pSceneMgr);
    m_pWorld->mPlayer2 = new Player("Player2", rOgreTb.m_pSceneMgr);
    m_pWorld->mPlayer1->mNode->translate(-100, 0, 0);
    m_pWorld->mPlayer2->mNode->translate(100, 0, 0);

    // Set the scene's ambient light
    rOgreTb.m_pSceneMgr->setAmbientLight(ColourValue(0.9f, 0.9f, 0.9f));
    Light* pointLight = rOgreTb.m_pSceneMgr->createLight("pointLight");
    pointLight->setType(Light::LT_POINT);
    pointLight->setPosition(Vector3(50, 100, 50));
    pointLight->setDiffuseColour(ColourValue::White);
    pointLight->setSpecularColour(ColourValue::White);
    pointLight->setCastShadows(true);

    // Skybox
    //rOgreTb.m_pSceneMgr->setSkyDome(true, "Terrain/Sky", 5, 8);

    // Fog
    Ogre::ColourValue fadeColour(1.0, 1.0, 1.0);
    rOgreTb.m_pSceneMgr->setFog(Ogre::FOG_LINEAR, fadeColour, 0.0, 500, 5000);

    // Create Disc
    Entity* discEnt = rOgreTb.m_pSceneMgr->createEntity("Disc", "sphere.mesh");
    discEnt->setMaterialName("Disc/Disc");
    SceneNode* disc = rOgreTb.m_pSceneMgr->createSceneNode("Disc");
    disc->setScale(0.08, 0.01, 0.08);
    disc->attachObject(discEnt);
    rOgreTb.m_pSceneMgr->getRootSceneNode()->addChild(disc);
    m_pWorld->mDisc->mOrientation = disc->getOrientation();

    // Create target
    Entity* targetEnt = rOgreTb.m_pSceneMgr->createEntity("Target", "cube.mesh");
    targetEnt->setMaterialName("Terrain/Target");
    SceneNode* target = rOgreTb.m_pSceneMgr->getRootSceneNode()->createChildSceneNode("Target");
    target->setScale(0.1, 0.1, 0.1);
    target->translate(0, -20, 0);
    target->attachObject(targetEnt);

    // Create Main Building
    Entity* buildingEnt = rOgreTb.m_pSceneMgr->createEntity("Building", "cube.mesh");
    buildingEnt->setMaterialName("Terrain/Building");
    SceneNode* building = rOgreTb.m_pSceneMgr->createSceneNode("Building");
    building->setScale(0.01 * TER_WIDTH, 10, 0.01 * TER_HEIGHT);
    building->translate(0, -510, 0);
    building->attachObject(buildingEnt);
    rOgreTb.m_pSceneMgr->getRootSceneNode()->addChild(building);


    // Create more buildings
    for (int i = 0; i < 100; i++)
    {
        char numstr[3];
        sprintf(numstr, "%d", i);
        Entity* buildingEnt = rOgreTb.m_pSceneMgr->createEntity(std::string("Building")+numstr, "cube.mesh");
        buildingEnt->setMaterialName("Disc/Disc");
        SceneNode* building = rOgreTb.m_pSceneMgr->createSceneNode(std::string("Building")+numstr);
        building->setScale(1+rand()%3, 10, 1+rand()%3);
        building->translate(-1000+rand()%2000, -511, -1000+rand()%2000);
        building->attachObject(buildingEnt);
        rOgreTb.m_pSceneMgr->getRootSceneNode()->addChild(building);
    }

    // Create Floor
    Entity* floorEnt = rOgreTb.m_pSceneMgr->createEntity("Floor", "cube.mesh");
    floorEnt->setMaterialName("Terrain/Floor");
    SceneNode* floor = rOgreTb.m_pSceneMgr->createSceneNode("Floor");
    floor->setScale(0.01 * TER_WIDTH, 0.1, 0.01 * TER_HEIGHT);
    floor->translate(0, -10, 0);
    floor->attachObject(floorEnt);
    rOgreTb.m_pSceneMgr->getRootSceneNode()->addChild(floor);
    floorEnt->setCastShadows(false);

    initGame(true);
}

// Update score on player objects and GUI
void Game::updateScore(bool inPlayer1)
{
    SceneNode* disc = OgreToolbox::getSingleton().m_pSceneMgr->getSceneNode("Disc");
    Player* inPlayer = (inPlayer1 ? m_pWorld->mPlayer2 : m_pWorld->mPlayer1);
    Player* outPlayer = (inPlayer1 ? m_pWorld->mPlayer1 : m_pWorld->mPlayer2);

    if (m_pWorld->mTimer < 0.0)
    {
        if ((inPlayer->score % 100) > (outPlayer->score % 100))
            inPlayer->score += 100;
        else if ((inPlayer->score % 100) < (outPlayer->score % 100))
            outPlayer->score += 100;
        else
        {
            inPlayer->score = (inPlayer->score / 100) * 100;
            outPlayer->score = (outPlayer->score / 100) * 100;
        }
    }

    // Disc dropped
    if (disc->getPosition().y < 0.0)
        inPlayer->score += 2;
    // Disc in side zones
    else if ((disc->getPosition().z < -TER_HEIGHT/6) || (disc->getPosition().z > TER_HEIGHT/6))
        inPlayer->score += 3;
    // Disc in middle zone
    else
        inPlayer->score += 5;

    if ((inPlayer->score % 100) >= 12)
    {
        m_pWorld->mPlayer1->score -= m_pWorld->mPlayer1->score % 100;
        m_pWorld->mPlayer2->score -= m_pWorld->mPlayer2->score % 100;
        inPlayer->score += 100;
    }


    if (inPlayer->score >= 200)
    {
        // TODO : Handle Victory Case
        m_pWorld->mPlayer1->score = 0;
        m_pWorld->mPlayer2->score = 0;
    }

    m_pWorld->mDisc->mSpeed = Ogre::Vector3(inPlayer1 ? -INIT_SPEED : INIT_SPEED, 0, 0);

}

void Game::initGame(bool initSet)
{
    SceneNode* disc = OgreToolbox::getSingleton().m_pSceneMgr->getSceneNode("Disc");

    if (!initSet)
        updateScore((disc->getPosition().x < 0.0) ? true : false);
    else
    {
        m_pWorld->mDisc->mSpeed = Ogre::Vector3(INIT_SPEED, 0, 0);
        m_pWorld->mTimer = 99.0;
    }

    disc->setPosition(0, 0, 0);
    m_pWorld->mDisc->state = DISC_DEFAULT;
}

bool Game::collision(Ogre::SceneNode* Pl, Ogre::SceneNode* Disc, bool flip)
{
    using namespace Ogre;
    Vector3 posPl = Pl->getPosition();
    Vector3 posDisc = Disc->getPosition();
    Real collOffset = (PLAYER_WIDTH + DISC_WIDTH) * MESH_SCALE / 2;

    if (!flip)
    {
        return (posPl.squaredDistance(posDisc) < pow(collOffset, 2));
    }
    else
    {
        if ((Pl == m_pWorld->mPlayer1->mNode ? m_pWorld->mDisc->mSpeed.x < 0.0 : m_pWorld->mDisc->mSpeed.x > 0.0)
            && m_pWorld->mDisc->state | DISC_LOB)
        {
            if ((posPl.squaredDistance(posDisc) < pow(collOffset + 10.0, 2))
                && (posPl.squaredDistance(posDisc) > pow(collOffset, 2)))
            {
                Pl->setPosition(posPl);

                return true;
            }
        }

        return false;
    }
}

bool Game::processUnbufferedInput(const Ogre::FrameEvent& evt, Player* player)
{
    using namespace Ogre;
    OIS::Keyboard* mKeyboard = m_pInputMgr->getKeyboard();
    Vector3& startLob = m_pWorld->mDisc->startLob;
    Vector3& endLob = m_pWorld->mDisc->endLob;
    Vector3& mSpeed = m_pWorld->mDisc->mSpeed;
    Player* mPlayer1 = m_pWorld->mPlayer1;
    Player* mPlayer2 = m_pWorld->mPlayer2;
    SceneNode* node = player->mNode;
    SceneNode* disc = OgreToolbox::getSingleton().m_pSceneMgr->getSceneNode("Disc");
    Real playerOffset = PLAYER_WIDTH * MESH_SCALE / 2;
    Real discOffset = DISC_WIDTH * MESH_SCALE / 2;

    if (!player->mHasDisc)
    {
        if (mKeyboard->isKeyDown(player->key_up))
        {
            if (node->getPosition().z > -TER_HEIGHT/2 + playerOffset)
                player->mSpeed.z = -player->maxSpeed;
            else
                player->mSpeed = Vector3(0, 0, 0);
        }
        if (mKeyboard->isKeyDown(player->key_down))
        {
            if (node->getPosition().z < TER_HEIGHT/2 - playerOffset)
                player->mSpeed.z = player->maxSpeed;
            else
                player->mSpeed = Vector3(0, 0, 0);
        }
        if (mKeyboard->isKeyDown(player->key_left))
        {
            if (node->getPosition().x > (player == mPlayer2 ? playerOffset : -TER_WIDTH/2 + playerOffset))
                player->mSpeed.x = -player->maxSpeed;
            else
                player->mSpeed = Vector3(0, 0, 0);
        }
        if (mKeyboard->isKeyDown(player->key_right))
        {
            if (node->getPosition().x < (player == mPlayer1 ? -playerOffset : TER_WIDTH/2 - playerOffset))
                player->mSpeed.x = player->maxSpeed;
            else
                player->mSpeed = Vector3(0, 0, 0);
        }
        if (mKeyboard->isKeyDown(player->key_throw) && !(m_pWorld->mDisc->state & DISC_LOB))
        {
            if (collision(player->mNode, disc, true))
            {
                //OgreToolbox::getSingletonPtr()->m_pLog->logMessage("FLIP");

                m_pWorld->mDisc->state |= DISC_FLIP;

                startLob.x = disc->getPosition().x;
                startLob.y = 0.0;
                startLob.z = disc->getPosition().z;

                endLob.x = disc->getPosition().x + (rand() % (FLIP_RAY * 2)) - FLIP_RAY;
                endLob.y = 0.0;
                endLob.z = findYCircle(FLIP_RAY, startLob.x, startLob.z, endLob.x);

                mSpeed.x = (startLob.x < endLob.x ? 1 : -1) * Vector2(startLob.x).distance(Vector2(endLob.x)) / 1.5;
                mSpeed.z = (startLob.z > endLob.z ? -1 : 1) * Vector2(startLob.z).distance(Vector2(endLob.z)) / 1.5;

                OgreToolbox::getSingleton().m_pSceneMgr->getSceneNode("Target")->setPosition(endLob.x, -6, endLob.z);

                m_pWorld->mDisc->state |= DISC_LOB;
                player->mHasDisc = false;
                player->discTimer = 0.0;
            }
        }
        player->mSpeed.normalise();
        player->mSpeed *= player->maxSpeed;
    }
    else
    {
        mSpeed.z = 0.0;
        if (mKeyboard->isKeyDown(player->key_up))
        {
            if (mKeyboard->isKeyDown(player->key_left))
                mSpeed.z = (player == mPlayer1 ? -player->backCoeffY : -player->forwCoeffY);
            else
                if (mKeyboard->isKeyDown(player->key_right))
                    mSpeed.z = (player == mPlayer1 ? -player->forwCoeffY : -player->backCoeffY);
                else
                    mSpeed.z = -player->midCoeff;
        }
        if (mKeyboard->isKeyDown(player->key_down))
        {
            if (mKeyboard->isKeyDown(player->key_left))
                mSpeed.z = (player == mPlayer1 ? player->backCoeffY : player->forwCoeffY);
            else
                if (mKeyboard->isKeyDown(player->key_right))
                    mSpeed.z = (player == mPlayer1 ? player->forwCoeffY : player->backCoeffY);
                else
                    mSpeed.z = player->midCoeff;
        }

        // Shots
        if (mKeyboard->isKeyDown(player->key_lob))
        {
            startLob.x = node->getPosition().x;
            startLob.y = 0.0;
            startLob.z = node->getPosition().z;
            endLob.x = (1 - player->discTimer) * (player == mPlayer1 ? TER_WIDTH / 2 : -TER_WIDTH / 2);
            endLob.y = 0.0;
            endLob.z = node->getPosition().z + mSpeed.z * ((TER_HEIGHT / 2) - discOffset);

            if (endLob.z < -(TER_HEIGHT / 2) + discOffset)
                endLob.z = -(TER_HEIGHT / 2) + discOffset;

            if (endLob.z > (TER_HEIGHT / 2) - discOffset)
                endLob.z = (TER_HEIGHT / 2) - discOffset;

            OgreToolbox::getSingleton().m_pSceneMgr->getSceneNode("Target")->setPosition(endLob.x, -6, endLob.z);

            mSpeed.x = (player == mPlayer1 ? 1 : -1) * Vector2(startLob.x).distance(Vector2(endLob.x)) / 1.5;
            mSpeed.z = (mSpeed.z > 0.0 ? 1 : -1 ) * Vector2(startLob.z).distance(Vector2(endLob.z)) / 1.5;

            m_pWorld->mDisc->state |= DISC_LOB;
            player->mHasDisc = false;
            player->discTimer = 0.0;
        }
        else
            if (player->discTimer > 1.0 || mKeyboard->isKeyDown(player->key_throw))
            {

                Real speed = MIN_SPEED + (1.0 - player->discTimer) * (MAX_SPEED - MIN_SPEED);
                mSpeed.x = (mSpeed.x > 0 ? -speed : speed);

                if (areEqualRel(fabs(mSpeed.z), fabs(player->backCoeffY), 0.0000001))
                    mSpeed.x *= player->backCoeffX;
                else if (areEqualRel(fabs(mSpeed.z), fabs(player->forwCoeffY), 0.0000001))
                    mSpeed.x *= player->forwCoeffX;
                else if (areEqualRel(fabs(mSpeed.z), fabs(player->midCoeff), 0.0000001))
                    mSpeed.x *= player->midCoeff;

                mSpeed.z *= speed;

                if (player->discTimer > 1.0)
                    mSpeed.z = 0.0;

                player->mHasDisc = false;
                player->discTimer = 0.0;
            }

        player->discTimer += evt.timeSinceLastFrame;
    }
    return true;
}

bool Game::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    using namespace Ogre;

    OgreToolbox& rOgreTb = OgreToolbox::getSingleton();
    SceneNode* p1 = m_pWorld->mPlayer1->mNode;
    SceneNode* p2 = m_pWorld->mPlayer2->mNode;
    SceneNode* disc = rOgreTb.m_pSceneMgr->getSceneNode("Disc");
    Real collOffset = (PLAYER_WIDTH + DISC_WIDTH) * MESH_SCALE / 2 + 3.0;

    if (mTraveling)
    {
        if (rOgreTb.m_pCamera->getPosition().z > 130)
        {
            rOgreTb.m_pCamera->move(Ogre::Vector3(0, 0, -300 * evt.timeSinceLastFrame));
            rOgreTb.m_pCamera->lookAt(Ogre::Vector3(0,0,0));
        }
        else
        {
            mPlaying = true;
            mTraveling = false;
        }
        if (rOgreTb.m_pCamera->getPosition().y > 255)
        {
            rOgreTb.m_pCamera->move(Ogre::Vector3(0, -100 * evt.timeSinceLastFrame, 0));
            rOgreTb.m_pCamera->lookAt(Ogre::Vector3(0,0,0));
        }
    }

    if (mPlaying)
    {
        processUnbufferedInput(evt, m_pWorld->mPlayer1);
        processUnbufferedInput(evt, m_pWorld->mPlayer2);

        p1->translate(m_pWorld->mPlayer1->mSpeed * evt.timeSinceLastFrame);
        p2->translate(m_pWorld->mPlayer2->mSpeed * evt.timeSinceLastFrame);

        // Disc bumps top or bottom field
        if ((disc->getPosition().z < -TER_HEIGHT/2) || (disc->getPosition().z > TER_HEIGHT/2))
        {
            m_pWorld->mDisc->mSpeed.z = -m_pWorld->mDisc->mSpeed.z;
            disc->translate(m_pWorld->mDisc->mSpeed * evt.timeSinceLastFrame);
        }

        // Player lost or time has run out
        if ((disc->getPosition().x < -TER_WIDTH/2) || (disc->getPosition().x > TER_WIDTH/2) || (m_pWorld->mTimer < 0.0))
        {
            if (m_pWorld->mTimer < 0.0)
                m_pWorld->mTimer = 99.0;

            initGame(false);
        }

        if (!m_pWorld->mPlayer1->mHasDisc && !m_pWorld->mPlayer2->mHasDisc)
        {
            // Move objects at mSpeed per second
            disc->translate(m_pWorld->mDisc->mSpeed * evt.timeSinceLastFrame);
            if (m_pWorld->mDisc->state & DISC_FLIP)
                disc->roll(Degree(evt.timeSinceLastFrame * 360));
            else
                disc->setOrientation(m_pWorld->mDisc->mOrientation);

            if (disc->getPosition().y < 10.0)
            {
                // Check disc/player collision
                if (collision(p1, disc, false))
                {
                    //OgreToolbox::getSingletonPtr()->m_pLog->logMessage("P1 got DISC");
                    m_pWorld->mDisc->mSpeed = Vector3(0,0,0);
                    disc->setPosition(Vector3(p1->getPosition().x + collOffset, 0, p1->getPosition().z));
                    disc->setOrientation(m_pWorld->mDisc->mOrientation);
                    m_pWorld->mPlayer1->mSpeed = Vector3(0, 0, 0);
                    m_pWorld->mPlayer1->mHasDisc = true;
                    m_pWorld->mDisc->state = DISC_DEFAULT;
                }
                else if (collision(p2, disc, false))
                {
                    //OgreToolbox::getSingletonPtr()->m_pLog->logMessage("P2 got DISC");
                    m_pWorld->mDisc->mSpeed = Vector3(m_pWorld->mDisc->mSpeed.x,0,0);
                    disc->setPosition(Vector3(p2->getPosition().x - collOffset, 0, p2->getPosition().z));
                    disc->setOrientation(m_pWorld->mDisc->mOrientation);
                    m_pWorld->mPlayer2->mSpeed = Vector3(0, 0, 0);
                    m_pWorld->mPlayer2->mHasDisc = true;
                    m_pWorld->mDisc->state = DISC_DEFAULT;
                }
            }

            if (m_pWorld->mDisc->state & DISC_LOB)
            {
                Vector3 temp = disc->getPosition();
                Real t = (m_pWorld->mDisc->startLob.x - temp.x) / (m_pWorld->mDisc->startLob.x - m_pWorld->mDisc->endLob.x);
                temp.y = 2 * (1 - t) * t * BEZIER;
                disc->setPosition(temp);
            }
            else
            {
                if (rOgreTb.m_pSceneMgr->getSceneNode("Target")->getPosition().y > -20)
                    rOgreTb.m_pSceneMgr->getSceneNode("Target")->setPosition(0, -20, 0);
            }

            if (disc->getPosition().y < 0.0)
            {
                initGame(false);
            }
        }

        m_pWorld->mTimer -= evt.timeSinceLastFrame;


    }

    // Move red city
    for (int i = 0; i < 100; i++)
    {
        char numstr[3];
        sprintf(numstr, "%d", i);
        SceneNode* b = rOgreTb.m_pSceneMgr->getSceneNode(std::string("Building")+numstr);
        b->setPosition(b->getPosition().x,
                       -600+sin(m_pWorld->mTimer+i)*10,
                       b->getPosition().z);
    }

    return true;
}

void Game::updateGame(double timeSinceLastFrame)
{
    (void) timeSinceLastFrame;
    m_pInputMgr->capture();
}

#if !((OGRE_PLATFORM == OGRE_PLATFORM_APPLE) && __LP64__)
void Game::runGame()
{
    //OgreToolbox::getSingletonPtr()->m_pLog->logMessage("Start main loop...");

    double timeSinceLastFrame = 0;
    double startTime = 0;

    OgreToolbox::getSingletonPtr()->m_pRenderWnd->resetStatistics();

    while(!m_bShutdown)
    {
        if(OgreToolbox::getSingletonPtr()->m_pRenderWnd->isClosed())
            m_bShutdown = true;

# if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || \
OGRE_PLATFORM == OGRE_PLATFORM_LINUX || \
OGRE_PLATFORM == OGRE_PLATFORM_APPLE
        Ogre::WindowEventUtilities::messagePump();
# endif
        if(OgreToolbox::getSingletonPtr()->m_pRenderWnd->isActive())
        {
            startTime = OgreToolbox::getSingletonPtr()->m_pTimer->getMillisecondsCPU();
            
            updateGame(timeSinceLastFrame);
            OgreToolbox::getSingletonPtr()->updateOgre(timeSinceLastFrame);
            OgreToolbox::getSingletonPtr()->m_pRoot->renderOneFrame();
            
            timeSinceLastFrame = OgreToolbox::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - startTime;
        }
        else
        {
# if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            Sleep(1000);
# else
            sleep(1);
# endif
        }
    }
    
    OgreToolbox::getSingletonPtr()->m_pLog->logMessage("Main loop quit");
    OgreToolbox::getSingletonPtr()->m_pLog->logMessage("Shutdown OGRE...");
}
#endif
