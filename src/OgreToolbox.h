#ifndef OGRE_TOOLBOX_H
# define OGRE_TOOLBOX_H

# include <OgreCamera.h>
# include <OgreEntity.h>
# include <OgreLogManager.h>
# include <OgreOverlay.h>
# include <OgreOverlayElement.h>
# include <OgreOverlayManager.h>
# include <OgreRoot.h>
# include <OgreViewport.h>
# include <OgreSceneManager.h>
# include <OgreRenderWindow.h>
# include <OgreConfigFile.h>
#include <OgreWindowEventUtilities.h>

# ifdef OGRE_STATIC_LIB
#   define OGRE_STATIC_GL
#   if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#     define OGRE_STATIC_Direct3D9
// dx10 will only work on vista, so be careful about statically linking
#     if OGRE_USE_D3D10
#       define OGRE_STATIC_Direct3D10
#     endif
#   endif
#   define OGRE_STATIC_CgProgramManager
#   ifdef OGRE_USE_PCZ
#     define OGRE_STATIC_PCZSceneManager
#     define OGRE_STATIC_OctreeZone
#   endif
#   include "OgreStaticPluginLoader.h"
# endif

class OgreToolbox : public Ogre::Singleton<OgreToolbox>
{
public:
    OgreToolbox();
    ~OgreToolbox();

    void updateOgre(double timeSinceLastFrame);

    Ogre::Root*                 m_pRoot;
    Ogre::Timer*                m_pTimer;
    Ogre::RenderWindow*         m_pRenderWnd;
    Ogre::SceneManager*         m_pSceneMgr;
    Ogre::Camera*               m_pCamera;
    Ogre::Viewport*             m_pViewport;
    Ogre::Log*                  m_pLog;

protected:
    // Added for Mac compatibility
    Ogre::String m_ResourcePath;

private:
    OgreToolbox(const OgreToolbox&);
    OgreToolbox& operator= (const OgreToolbox&);

    void moveCamera();

    Ogre::FrameEvent m_FrameEvent;
    int              m_iNumScreenShots;

    Ogre::Vector3 m_TranslateVector;
    Ogre::Real    m_MoveSpeed;
    Ogre::Degree  m_RotateSpeed;
    float         m_MoveScale;
    Ogre::Degree  m_RotScale;
# ifdef OGRE_STATIC_LIB
    Ogre::StaticPluginLoader m_StaticPluginLoader;
# endif

    friend class Game;
};

#endif /* OGRE_TOOLBOX_H */
