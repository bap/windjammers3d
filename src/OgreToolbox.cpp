#include "OgreToolbox.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
# include "macUtils.h"
#endif
using namespace Ogre;

namespace Ogre
{
    template<> OgreToolbox* Ogre::Singleton<OgreToolbox>::msSingleton = 0;
};

OgreToolbox::OgreToolbox()
    : m_pRoot      (0),
      m_pTimer     (0),
      m_pRenderWnd (0),
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
      m_ResourcePath (macBundlePath() + "/Contents/Resources/"),
#else
      m_ResourcePath ("../config/"),
#endif
      m_pSceneMgr  (0),
      m_pCamera    (0),
      m_pViewport  (0),
      m_pLog       (0),
      m_FrameEvent      (Ogre::FrameEvent ()),
      m_iNumScreenShots (0),
      m_TranslateVector (Ogre::Vector3 ()),
      m_MoveSpeed       (3.0f),
      m_RotateSpeed     (0.3f),
      m_MoveScale       (0.0f),
      m_RotScale        (0.0f)
{
}

OgreToolbox::~OgreToolbox()
{
#ifdef OGRE_STATIC_LIB
    m_StaticPluginLoader.unload();
#endif
    if(m_pRoot)
        delete m_pRoot;
}

void OgreToolbox::updateOgre(double timeSinceLastFrame)
{
    m_MoveScale = m_MoveSpeed   * (float)timeSinceLastFrame;
    m_RotScale  = m_RotateSpeed * (float)timeSinceLastFrame;

#if OGRE_VERSION >= 0x10800
    m_pSceneMgr->setSkyBoxEnabled(true);
#endif

    m_TranslateVector = Vector3::ZERO;

    moveCamera();

    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
}

void OgreToolbox::moveCamera()
{
   m_pCamera->moveRelative(m_TranslateVector);
}

// void OgreToolbox::getInput()
// {
//     if(m_pKeyboard->isKeyDown(OIS::KC_A))
//             m_TranslateVector.x = -m_MoveScale;
//
//     if(m_pKeyboard->isKeyDown(OIS::KC_D))
//             m_TranslateVector.x = m_MoveScale;
//
//     if(m_pKeyboard->isKeyDown(OIS::KC_W))
//             m_TranslateVector.z = -m_MoveScale;
//
//     if(m_pKeyboard->isKeyDown(OIS::KC_S))
//             m_TranslateVector.z = m_MoveScale;
// }
