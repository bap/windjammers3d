#ifndef __Player_h_
#define __Player_h_

#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreVector3.h>
#include <OgreSceneManager.h>
#include <OISKeyboard.h>
#include <string>

class Player
{
public:
    Player(std::string name, Ogre::SceneManager *sceneMgr);
    virtual ~Player(void);
    std::string scoreString(void);

public:
    std::string     name;
    Ogre::Vector3   mSpeed;
    Ogre::Entity    *mEntity;
    Ogre::SceneNode *mNode;
    bool            mHasDisc;
    bool            flip;
    float           discTimer;
    float           maxSpeed;
    int             score;
    float           backCoeffX;
    float           backCoeffY;
    float           midCoeff;
    float           forwCoeffX;
    float           forwCoeffY;
    OIS::KeyCode    key_up;
    OIS::KeyCode    key_down;
    OIS::KeyCode    key_left;
    OIS::KeyCode    key_right;
    OIS::KeyCode    key_throw;
    OIS::KeyCode    key_lob;
};

#endif // #ifndef __Player_h_
