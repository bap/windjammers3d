#include "Player.h"
#include <sstream>

Player::Player(std::string name, Ogre::SceneManager *sceneMgr)
  : name(name),
    mSpeed(Ogre::Vector3(0, 0, 0)),
    mEntity(0),
    mNode(0),
    mHasDisc(false),
    flip(false),
    discTimer(0.0),
    maxSpeed(130.0),
    score(0)
{
  mNode = sceneMgr->getRootSceneNode()->createChildSceneNode(name);

    backCoeffX = 2.0 / sqrt(13.0);
    backCoeffY = 3.0 / sqrt(13.0);
    midCoeff = 1.0 / sqrt(2.0);
    forwCoeffX = 2.0 / sqrt(5.0);
    forwCoeffY = 1.0 / sqrt(5.0);

  if (name == "Player1")
  {
    mEntity = sceneMgr->createEntity(name, "ship.mesh");
    mEntity->setMaterialName("Player/Player1");
    mNode->roll(Ogre::Degree(-90));
    mNode->yaw(Ogre::Degree(90));
  }
  else
  {
    mEntity = sceneMgr->createEntity(name, "ship2.mesh");
    mEntity->setMaterialName("Player/Player2");
    mNode->roll(Ogre::Degree(90));
    mNode->yaw(Ogre::Degree(-90));
  }

  mNode->setScale(0.2, 0.2, 0.2);
  mNode->attachObject(mEntity);

  if (name == "Player2")
  {
    key_up = OIS::KC_UP;
    key_down = OIS::KC_DOWN;
    key_left = OIS::KC_LEFT;
    key_right = OIS::KC_RIGHT;
    key_throw = OIS::KC_P;
    key_lob = OIS::KC_O;
  }
  else
  {
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE
    key_up = OIS::KC_Z;
    key_down = OIS::KC_S;
    key_left = OIS::KC_Q;
    key_right = OIS::KC_D;
#else
    key_up = OIS::KC_W;
    key_down = OIS::KC_S;
    key_left = OIS::KC_A;
    key_right = OIS::KC_D;
#endif
    key_throw = OIS::KC_SPACE;
    key_lob = OIS::KC_B;
  }
}

Player::~Player(void)
{
}

std::string Player::scoreString(void)
{
  std::stringstream ss;
  ss << name << " : " << score / 100 << " | " << score % 100;
  return ss.str();
}
