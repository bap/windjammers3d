#ifndef INPUT_MANAGER_H_
# define INPUT_MANAGER_H_

# include <OISInputManager.h>

# include "Keyboard.h"
# include "Mouse.h"

namespace Input
{
    class InputManager
    {
        public:
            InputManager  (OIS::ParamList paramList);
            ~InputManager ();

            void createKeyboard ();
            void createMouse    (unsigned int width, unsigned int height);

            void capture ();
            void mapKey (OIS::KeyCode key, keyHandler onPress, keyHandler onRelease);

            OIS::Keyboard* getKeyboard() { return m_pKeyboard; }
            OIS::Mouse*    getMouse()    { return m_pMouse; }

        private:
            OIS::InputManager *m_pInputMgr;
            OIS::Keyboard     *m_pKeyboard;
            OIS::Mouse        *m_pMouse;

            Keyboard *m_pKeyListener;
            Mouse    *m_pMouseListener;
    };
}

#endif /* !INPUT_MANAGER_H_ */
