#ifndef MOUSE_H_
# define MOUSE_H_

# include <OISMouse.h>

# include "../OgreToolbox.h"

namespace Input
{
    class Mouse : public OIS::MouseListener
    {
    public:
        Mouse  ();
        ~Mouse ();

        bool mouseMoved    (const OIS::MouseEvent &evt);
        bool mousePressed  (const OIS::MouseEvent &evt, OIS::MouseButtonID id);
        bool mouseReleased (const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    };
}

#endif /* !MOUSE_H_ */
