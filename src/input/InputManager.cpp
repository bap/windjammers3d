#include "InputManager.h"

namespace Input
{
    InputManager::InputManager (OIS::ParamList paramList)
        : m_pInputMgr (OIS::InputManager::createInputSystem(paramList)),
          m_pKeyboard (0),
          m_pMouse (0),
          m_pKeyListener (0),
          m_pMouseListener (0)
    {
    }

    InputManager::~InputManager ()
    {
        OIS::InputManager::destroyInputSystem(m_pInputMgr);
    }

    void InputManager::createKeyboard ()
    {
        m_pKeyboard = static_cast<OIS::Keyboard*>(m_pInputMgr->createInputObject(OIS::OISKeyboard, true));
        m_pKeyListener = new Keyboard();

        m_pKeyboard->setEventCallback(m_pKeyListener);
    }

    void InputManager::createMouse (unsigned int width, unsigned int height)
    {
        m_pMouse = static_cast<OIS::Mouse*>(m_pInputMgr->createInputObject(OIS::OISMouse, true));
        m_pMouseListener = new Mouse();

        m_pMouse->getMouseState().width  = width;
        m_pMouse->getMouseState().height = height;

        m_pMouse->setEventCallback(m_pMouseListener);
    }

    void InputManager::capture ()
    {
        m_pKeyboard->capture();
        m_pMouse->capture();
    }

    void InputManager::mapKey (OIS::KeyCode key,
                               keyHandler onPress,
                               keyHandler onRelease)
    {
        handlerParams handlers = { onPress, onRelease };
        m_pKeyListener->m_keyMap[key] = handlers;
    }
}
