#include "Mouse.h"

namespace Input
{
    Mouse::Mouse ()
    {
    }

    Mouse::~Mouse ()
    {
    }

    bool Mouse::mouseMoved(const OIS::MouseEvent &evt)
    {
        OgreToolbox::getSingleton().m_pCamera->yaw(Ogre::Degree(evt.state.X.rel * -0.1f));
        OgreToolbox::getSingleton().m_pCamera->pitch(Ogre::Degree(evt.state.Y.rel * -0.1f));

        return true;
    }


    bool Mouse::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
    {
        return true;
    }

    bool Mouse::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
    {
        return true;
    }
}
