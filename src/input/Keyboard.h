#ifndef KEYBOARD_H_
# define KEYBOARD_H_

# include <map>
# include <OISKeyboard.h>

# include "../OgreToolbox.h"

class World;

namespace Input
{
    typedef void (*keyHandler) (World *world);

    struct handlerParams
    {
        keyHandler onPress;
        keyHandler onRelease;
    };

    class Keyboard : public OIS::KeyListener
    {
    public:
        Keyboard  ();
        ~Keyboard ();

        bool keyPressed  (const OIS::KeyEvent &keyEventRef);
        bool keyReleased (const OIS::KeyEvent &keyEventRef);

    private:
        std::map<OIS::KeyCode, handlerParams> m_keyMap;

        friend class InputManager;
    };
}

#endif /* !KEYBOARD_H_ */
