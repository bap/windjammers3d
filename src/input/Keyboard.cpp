#include "Keyboard.h"
#include "../Game.h"

namespace Input
{
    Keyboard::Keyboard ()
    {
    }

    Keyboard::~Keyboard ()
    {
    }

    bool Keyboard::keyPressed (const OIS::KeyEvent &keyEventRef)
    {
        if (m_keyMap.find(keyEventRef.key) != m_keyMap.end())
        {
            keyHandler handler = m_keyMap[keyEventRef.key].onPress;
            if (handler)
                handler(Game::getSingleton().m_pWorld);
        }
        return true;
    }

    bool Keyboard::keyReleased (const OIS::KeyEvent &keyEventRef)
    {
        if (m_keyMap.find(keyEventRef.key) != m_keyMap.end())
        {
            keyHandler handler = m_keyMap[keyEventRef.key].onRelease;
            if (handler)
                handler(Game::getSingleton().m_pWorld);
        }
        return true;
    }
}
