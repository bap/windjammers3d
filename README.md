# Windjammers 3D

## Compiling on Linux

    mkdir _build
    cd _build
    cmake ..
    make
    ./OgreApp

If you don't want the GUI you can call cmake with `-DCEGUI_ENABLED=OFF`.

## Compiling on Mac OS X

    * Create a new project from the Ogre3D XCode template
    * Add the Ogre framework to the linked framework list
    * In the build settings, set the OGRE_SDK_ROOT variable to the Ogre SDK
    path ("Add Build Setting" button in the bottom right corner)
    * In the run script section of the build rules setting, edit the script to
    copy the entire "config" folder instead of the *.cfg files
    * Backup the .plist, .pch and optionally the localization files (.strings)
    * Remove all source code files from the project
    * Copy the git repository in your project folder
    * Add all files to the project and resolve the missing references
    * Put back the backed up files
    * Compile !

    Note: You may have to fix the PluginFolder variable (plugins.cfg) or the
    resources location (resources.cfg) to the appropriate path

## Compiling on Windows

Not yet sorry!
